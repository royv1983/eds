﻿using EtatSituation.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace EtatSituation.Controllers
{
    public class ErreurController
    {
        #region Methodes Publiques

        public static long EnregistrerErreur(ErreurModel err)
        {
            SqlCommand cmd = new SqlCommand();
            string strcmd = "Insert Into Erreurs([Date],[Source],[NoErreur],[Message],[Pile],CreationUsager,AdresseIp,Noeud) Values(@Date,@Source,@NoErreur,@Message,@Pile,@CreationUsager,@AdresseIp,@Noeud)";
            cmd = new SqlCommand(strcmd);
            cmd.Parameters.Add(new SqlParameter("@Date", System.Data.SqlDbType.DateTime));
            cmd.Parameters["@Date"].Value = err.Date;

            cmd.Parameters.Add(new SqlParameter("@Source", System.Data.SqlDbType.Text));
            cmd.Parameters["@Source"].Value = err.Source;

            cmd.Parameters.Add(new SqlParameter("@NoErreur", System.Data.SqlDbType.Int));
            cmd.Parameters["@NoErreur"].Value = err.NoErreur;

            cmd.Parameters.Add(new SqlParameter("@Message", System.Data.SqlDbType.Text));
            cmd.Parameters["@Message"].Value = err.Message;

            cmd.Parameters.Add(new SqlParameter("@Pile", System.Data.SqlDbType.Text));
            cmd.Parameters["@Pile"].Value = err.Pile;

            cmd.Parameters.Add(new SqlParameter("@CreationUsager", System.Data.SqlDbType.VarChar, 10));
            cmd.Parameters["@CreationUsager"].Value = err.CreationUsager;

            cmd.Parameters.Add(new SqlParameter("@AdresseIp", System.Data.SqlDbType.VarChar, 20));
            cmd.Parameters["@AdresseIp"].Value = err.AdresseIp;

            cmd.Parameters.Add(new SqlParameter("@Noeud", System.Data.SqlDbType.VarChar, 50));
            cmd.Parameters["@Noeud"].Value = err.Noeud;


            //return SqlAssistantController.SqlInsertAndReturnId(cmd);
            return 0;
        }

        #endregion
    }
}