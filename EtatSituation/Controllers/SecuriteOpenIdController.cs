﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Policy;
using System.Web;
using EtatSituation.Models.SecuriteOpenID;
using Microsoft.Owin.Security;
using Newtonsoft.Json;
using RestSharp;
using System.Security.Claims;

namespace EtatSituation.Controllers
{
    public class SecuriteOpenIdController
    {
        public UtilisateurUS GetUtilisateur(string code)
        {
            try
            {
                //GET TOKEN******************************
                var clientToken = new RestClient(new Uri(ConfigurationManager.AppSettings["OicAuthorityURL"] + "oauth2/token"));
                var requestToken = new RestRequest(Method.POST)
                {
                    RequestFormat = DataFormat.Json
                };

                requestToken.AddHeader("Authorization", "Basic " + Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(string.Format("{0}:{1}", ConfigurationManager.AppSettings["OicClientId"], ConfigurationManager.AppSettings["OicClientSecret"]))));
                requestToken.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                requestToken.AddHeader("Accept", "application/json");
                requestToken.AddParameter("client_id", ConfigurationManager.AppSettings["OicClientId"]);
                requestToken.AddParameter("client_secret", ConfigurationManager.AppSettings["OicClientSecret"]);
                requestToken.AddParameter("code", code);
                requestToken.AddParameter("grant_type", "authorization_code");
                requestToken.AddParameter("redirect_uri", ConfigurationManager.AppSettings["OicRedirectURL"]);

                IRestResponse responseToken = clientToken.Execute(requestToken);
                TokenUS _token = JsonConvert.DeserializeObject<TokenUS>(responseToken.Content);

                var client = new RestClient(ConfigurationManager.AppSettings["OicAuthorityURL"] + "oauth2/userinfo");
                var request = new RestRequest(Method.GET);
                request.AddHeader("Authorization", "Bearer " + _token.access_token);
                IRestResponse response = client.Execute(request);

                UtilisateurUS _user = JsonConvert.DeserializeObject<UtilisateurUS>(response.Content);
                _user.AccessToken = _token.access_token;



                //find common place to keep below
                ClaimsPrincipal cp = HttpContext.Current.GetOwinContext().Authentication.User;
                 
                foreach (var uidentity in cp.Identities)
                {
                    var idTokenClaim = uidentity.FindFirst("id_token");
                    if (idTokenClaim != null)
                    {
                        uidentity.RemoveClaim(idTokenClaim);
                        uidentity.AddClaim(new Claim("id_token", _token.id_token));
                    }
                    else
                    {
                        uidentity.AddClaim(new Claim("id_token", _token.id_token));
                    }
                    var accessTokenClaim = uidentity.FindFirst("access_token");
                    if (accessTokenClaim != null)
                    {
                        uidentity.RemoveClaim(accessTokenClaim);
                        uidentity.AddClaim(new Claim("access_token", _token.access_token));
                    }
                    else
                    {
                        uidentity.AddClaim(new Claim("access_token", _token.access_token));
                    }
                    //var refreshTokenClaim = uidentity.FindFirst("refresh_token");
                    //if (refreshTokenClaim != null)
                    //{
                    //    uidentity.RemoveClaim(refreshTokenClaim);
                    //    uidentity.AddClaim(new Claim("refresh_token", _token.refresh_token));
                    //}
                    //else
                    //{
                    //    uidentity.AddClaim(new Claim("refresh_token", _token.refresh_token));
                    //}

                   
                    //var claims = new List<Claim>();
                    //claims.AddRange(userInfoResponse.Claims);
                    //claims.Add(new Claim("id_token", _token.id_token));
                    //claims.Add(new Claim("access_token", _token.access_token));



                    //HttpContext.Current.GetOwinContext().Authentication.AuthenticationTicket.Identity.AddClaims(claims);

                    HttpContext.Current.GetOwinContext().Authentication.SignIn(new AuthenticationProperties() { IsPersistent = true }, uidentity);
                }
                string s = "authentication manager type: {" + HttpContext.Current.GetOwinContext().Authentication.GetType().ToString();
                 s += "authenticated: {" + HttpContext.Current.GetOwinContext().Authentication.User.Identity.IsAuthenticated.ToString();
                 s += "user name: {" +  HttpContext.Current.GetOwinContext().Authentication.User.Identity.Name.ToString();



                // authenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = true }, userIdentity);
                return _user;
               
            }
            catch
            {
                return null;
            }
        }

        public UtilisateurUS GetUtilisateurToken(string code)
        {
            try
            {

                var client = new RestClient(ConfigurationManager.AppSettings["OicAuthorityURL"] + "oauth2/userinfo");
                var request = new RestRequest(Method.GET);
                request.AddHeader("Authorization", "Bearer " + code);
                IRestResponse response = client.Execute(request);

                UtilisateurUS _user = JsonConvert.DeserializeObject<UtilisateurUS>(response.Content);

                return _user;
            }
            catch
            {
                return null;
            }
        }


        public void Authentification()
        {
            //string provider = "authorization_code";
            //// Demande de reconnexion au fournisseur de connexion externe
            //string redirectUrl = Url.Action("Home", "Index", null);// ResolveUrl(String.Format(CultureInfo.InvariantCulture, "~/Account/RegisterExternalLogin?{0}={1}&returnUrl={2}", IdentityHelper.ProviderNameKey, provider, null));
            //var properties = new AuthenticationProperties() { RedirectUri = redirectUrl };
            //// Ajouter une vérification xsrf lors de la liaison de comptes
            //if (HttpContext.User.Identity.IsAuthenticated)
            //{
            //    properties.Dictionary[IdentityHelper.XsrfKey] = HttpContext.User.Identity.GetUserId();
            //}
            //HttpContext.GetOwinContext().Authentication.Challenge(properties, provider);

        }
    }
}