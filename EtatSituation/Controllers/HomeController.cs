﻿using EtatSituation.Models;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace EtatSituation.Controllers
{
    public class HomeController : Controller
    {
        
        //List<EdsActionModel> lstAction = null;
        //EdsActionPreventiveModel actionPrev = null;
        //List<DelaiModel> lstDelais = null;
       // [Authorize]
        public ActionResult Index(String id)
        {

            if (!HttpContext.User.Identity.IsAuthenticated)
            {

               string provider = "authorization_code";
              //  // Demande de reconnexion au fournisseur de connexion externe
              //  string redirectUrl = Url.Action("Home", "Index", null);// ResolveUrl(String.Format(CultureInfo.InvariantCulture, "~/Account/RegisterExternalLogin?{0}={1}&returnUrl={2}", IdentityHelper.ProviderNameKey, provider, null));
              //  var properties = new AuthenticationProperties() { RedirectUri = redirectUrl };
                   
              //   HttpContext.GetOwinContext().Authentication.Challenge(properties, provider);


              ////  HttpContext.GetOwinContext().Authentication.Challenge(OpenIdConnectAuthenticationDefaults.AuthenticationType);
              //  return new HttpUnauthorizedResult();
            }


            var i = id;
            if (((System.Web.HttpRequestWrapper)Request).Url.Query.Contains("code"))
            {
                Char delimiter = '&';
                var code = ((System.Web.HttpRequestWrapper)Request).Url.Query.Replace("code=", "").Split(delimiter);
                SecuriteOpenIdController securite = new SecuriteOpenIdController();
                var _user = securite.GetUtilisateur(code[2]);
                //Session["Token"] = _user.AccessToken;
                //Session["code_user"] = _user.CdUsager;
                //Session["groups"] = _user.groups;
                HttpContext.Session["Token"] = _user.AccessToken;
                HttpContext.Session["code_user"] = _user.CdUsager;
                //HttpContext.Session["groups"] = _user.groups;

                //var settings = (System.Collections.Specialized.NameValueCollection)System.Configuration.ConfigurationManager.GetSection("ADGroupeSecurite/ConfigSecurite") as System.Collections.Specialized.NameValueCollection;
                //var LstDroit = "";
                //if (settings != null)
                //{
                //    foreach (string key in settings.AllKeys)
                //    {
                //        if (_user.groups.ToString().Contains(key + ";"))
                //            LstDroit += (settings[key] + ";");
                //    }
                //}
                //Session["ListeDroit"] = LstDroit;




                    // AuthenticationTicket.Identity.AddClaims(claims);


                }
                if (((System.Web.HttpRequestWrapper)Request).Url.Query.Contains("access_token"))
            {
                Char delimiter = '&';
                var code = ((System.Web.HttpRequestWrapper)Request).Url.Query.Replace("?access_token=", "").Split(delimiter);
                SecuriteOpenIdController securite = new SecuriteOpenIdController();
                var _user = securite.GetUtilisateurToken(code[0]);
                //Session["Token"] = code[0];
                //Session["code_user"] = _user.CdUsager;
                //Session["groups"] = _user.groups;

                HttpContext.Session["Token"] = code[0];
                HttpContext.Session["code_user"] = _user.CdUsager;
                HttpContext.Session["groups"] = _user.groups;

                var settings = (System.Collections.Specialized.NameValueCollection)System.Configuration.ConfigurationManager.GetSection("ADGroupeSecurite/ConfigSecurite") as System.Collections.Specialized.NameValueCollection;
                var LstDroit = "";
                if (settings != null)
                {
                    foreach (string key in settings.AllKeys)
                    {
                        if (_user.groups.ToString().Contains(key + ";"))
                            LstDroit += (settings[key] + ";");
                    }
                }
                Session["ListeDroit"] = LstDroit;
            }
            //if (HttpContext.Session["Token"] == null)

            //{
            //    string provider = "authorization_code";
            //    // Demande de reconnexion au fournisseur de connexion externe
            //    string redirectUrl = Url.Action("Home", "Index", null);// ResolveUrl(String.Format(CultureInfo.InvariantCulture, "~/Account/RegisterExternalLogin?{0}={1}&returnUrl={2}", IdentityHelper.ProviderNameKey, provider, null));
            //    var properties = new AuthenticationProperties() { RedirectUri = redirectUrl };
            //    // Ajouter une vérification xsrf lors de la liaison de comptes
            //    if (HttpContext.User.Identity.IsAuthenticated)
            //    {
            //        properties.Dictionary[IdentityHelper.XsrfKey] = HttpContext.User.Identity.GetUserId();
            //    }
            //    HttpContext.GetOwinContext().Authentication.Challenge(properties, provider);
            //    //
            //    return View(new Models.IndexViewModel());
            //}
            ////IndexViewModel objIndex = new IndexViewModel { Titre = "Tableau de bord", ActionPreventive = actionPrev, Actions = lstAction, Delais = lstDelais };
            ////return View(objIndex);
            //else
            //{
          
            if (HttpContext.Session["Token"] == null)
            {
                 HttpContext.Session["code_user"] = "C VIDE";
                HttpContext.Session["Token"] = "C VIDE";
                string provider = "authorization_code";
                // Demande de reconnexion au fournisseur de connexion externe
                string redirectUrl = Url.Action("Home", "Index", null);// ResolveUrl(String.Format(CultureInfo.InvariantCulture, "~/Account/RegisterExternalLogin?{0}={1}&returnUrl={2}", IdentityHelper.ProviderNameKey, provider, null));
                var properties = new AuthenticationProperties() { RedirectUri = redirectUrl };
                // Ajouter une vérification xsrf lors de la liaison de comptes
                if (HttpContext.User.Identity.IsAuthenticated)
                {
                    properties.Dictionary[IdentityHelper.XsrfKey] = HttpContext.User.Identity.GetUserId();
                }
                HttpContext.GetOwinContext().Authentication.Challenge(properties, provider);
                //    //
                //    return View(new Models.IndexViewModel());
            }
            
                

            string user = HttpContext.Session["code_user"].ToString();
                string token = HttpContext.Session["Token"].ToString();


                Models.IndexViewModel indexVM = new Models.IndexViewModel();

                List<string> Securite = new List<string>();

                indexVM.User = user;
                indexVM.Token = token;



                return View(indexVM);



            //}
        }

        
    }
}