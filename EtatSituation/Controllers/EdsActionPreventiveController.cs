﻿using EtatSituation.Models;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;
using EtatSituation.API;
using System.Web.Mvc;
using System.Threading.Tasks;

namespace EtatSituation.Controllers
{
    public class EdsActionPreventiveController : Controller
    {
        // GET: EdsActionPreventive
        
        static EdsActionPreventiveModel actionPreventive = new EdsActionPreventiveModel();
        static List<DelaiModel> lstDelais = new List<DelaiModel>();
        static string codeUser = "";
        [Authorize]

        public ActionResult Index()
        {

            string user = "CHU VIDE";
                if (HttpContext.Session["code_user"] != null)
                {
                    user = HttpContext.Session["code_user"].ToString();
                }


                // List<string> Securite = new List<string>();
                LoadActionPreventive();
                List<EdsActionModel> lstAction = Eds.ObtenirActionsEDS();
                List<EdsPrioriteModel> lstPriorite = Eds.ObtenirListePriorite();
                //List<EdsActionModel> lstAction = new List<EdsActionModel>();
                //List<EdsPrioriteModel> lstPriorite = new List<EdsPrioriteModel>();
                IndexViewModel objIndex = new IndexViewModel { Titre = "Mesures d'action de protection", ActionPreventive = actionPreventive, Actions = lstAction, Delais = actionPreventive.ListeDelai, Priorite = lstPriorite, User = user };
                return View(objIndex);
            
            
        }
        /**
         * Méthode LoadActionPreventive sert chargeer les données de l'action préventive actif
         */
        public JsonResult LoadActionPreventive()
        {
            ////Actions Préventives - Aucune action preventive active
            bool result = false;
            EdsActionPreventiveModel actionPrev = Eds.ObtenirActionPreventiveActive();
            if (actionPrev.DescriptionAction != null)
            {
                result = true;
            }
            //Action preventive active et Verifier l'heure fin de l'action preventive active
            if (actionPrev != null && actionPrev.Statut == false)
            {
                actionPreventive = actionPrev;
            }

            if (result)
            {
                return Json(new { success = result, responseTitle = "Succès", responseText = "Actif" }, JsonRequestBehavior.AllowGet);

            }
            else
            {
                return Json(new { success = result, responseTitle = "Échec", responseText = "Inactif" }, JsonRequestBehavior.AllowGet);

            }

        }

        /**
         * Méthode ChargementDonnees sert charger les données de la barre déroulante.
         */
        public async Task<JsonResult> ChargementDonnees()
        {
            List<EdsDonnees> result = Eds.ObtenirDonneesEDS2("911");

            return Json(new
            {
                ActionPreventive = result.Where(a => a.Indicateur.Contains("ActionPreventive")).ToList(),
                QuotaEffectifs = result.Where(a => a.Indicateur.Contains("QuotaEffectifs")).ToList()
            });

        }

        public JsonResult ActionPreventive()
        {
            try
            {
                LoadActionPreventive();
                List<DelaiModel> _delais = new List<DelaiModel>();
                string _descAction = "";
                string _dateDebut = "";
                bool active = actionPreventive.Statut;

                    active = true;
                    int ActPrev = actionPreventive.ID;
                    int Action = actionPreventive.IDAction;
                    _dateDebut = actionPreventive.HeureDebut.TimeOfDay.Hours.ToString() + ":" + actionPreventive.HeureDebut.TimeOfDay.Minutes.ToString().PadLeft(2, '0');
                    _delais = actionPreventive.ListeDelai;
                    _descAction = actionPreventive.DescriptionAction;

                return Json(new
                {
                    actionActive = active,
                    listeDelais = _delais,
                    DescriptionAction = _dateDebut + " - " + _descAction,
                });

            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /**
        * Méthode FinaliserActionPreventive sert a terminer l'action de prévention actif.
        */
        public JsonResult FinaliserActionPreventive()
        {
            actionPreventive.Statut = true;
            actionPreventive.DtMaj = DateTime.Now;
            actionPreventive.HeureFin = DateTime.Now;
            actionPreventive.ResponsableMaj = codeUser;

           



            bool result =  Eds.MAJActionPreventive(actionPreventive);
            if (result)
            {
                return Json(new { success = result, responseTitle = "Succès", responseText = "Action de protection mis à jour!." }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                //  Send "Success"
                return Json(new { success = result, responseTitle = "Échec", responseText = "Action de proctection n\'a pas pu être mis à jour!" }, JsonRequestBehavior.AllowGet);
            }
        }


        /**
        * Méthode SaveActionPreventive sert ajouter une action preventive avec les données enregistrer.
        */
        public JsonResult SaveActionPreventive(int _IDAction,string _DescAction, string _HeureDebut, string _HeureFin, string _ListDelais,bool _majActionPrev)
        {
            EdsActionPreventiveModel actionPrev = new EdsActionPreventiveModel();

            actionPrev.ListeDelai = new List<DelaiModel>();
            actionPrev.IDAction = _IDAction;
            actionPrev.DescriptionAction = _DescAction;
            actionPrev.HeureDebut = Convert.ToDateTime(_HeureDebut);
            actionPrev.HeureFin = Convert.ToDateTime(_HeureDebut);
           
            actionPrev.Statut = false;
            actionPrev.DtCreation = DateTime.Now;
            actionPrev.ResponsableCreation = codeUser; // Faut mettre la securité pour trouver le user connecter
            actionPrev.DtMaj = null;
            actionPrev.ResponsableMaj = "";
            string[] lst = _ListDelais.Split(';');

                for (int i = 1; i < lst.Length; i++)
                {
               
                    int _Priorite = Convert.ToInt32(lst[i].Substring(2, 2));
                   // DateTime _DelaisPriorite = Conversion(lst[i].Split('>')[1]);
                    DateTime _DelaisPriorite = Convert.ToDateTime(lst[i].Split('>')[1]);

                    DelaiModel delais = new DelaiModel
                    {                     
                        IDActionPreventive = _IDAction,
                        Priorite = _Priorite,
                        DelaisPriorite = _DelaisPriorite,
                        DtCreation = DateTime.Now
                    };
                actionPrev.HeureFin += (_DelaisPriorite.TimeOfDay);
             
                    


                actionPrev.ListeDelai.Add(delais);
                lstDelais.Add(delais);
                   // TimeSpan temps = new TimeSpan(_DelaisPriorite.Hour);
                 //   actionPrev.HeureFin += temps;
                }


            //Eds.AjouterActionPreventive(actionPrev);
            
            bool result  = Eds.AjouterActionPreventive(actionPrev);
            if (result)
            {
               return Json(new { success = result, responseTitle = "Succès", responseText = "Action de protection ajouté!." }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                //  Send "Success"
                return Json(new { success = result,responseTitle = "Échec", responseText = "Action de proctection n\'a pas pu être ajouté!" }, JsonRequestBehavior.AllowGet);
            }            
        }
        
        /*
         Méthode Conversion sert a convertir les heures qui depassent 24h en jours
         et retourn le nouveau date time.
         */
        public DateTime Conversion(string temps)
        {
            string[] lstHeure = temps.Split(':');
            int jours = 0 ,
                heure = Convert.ToInt32(lstHeure[0]),
                minute = Convert.ToInt32(lstHeure[1]);
            if (minute >= 60)
            {
                int heurePlus = 0;
                heurePlus = minute / 60;
                minute = minute - (heurePlus * 60);
                heure += heurePlus;
            }
            if (heure >= 24)
            {
                jours = heure / 24;
                heure = heure - (jours * 24);
            }

            return new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day+jours,heure,minute,0);
        }

        /**
        * Méthode ModifierDelaisActionPreventive sert à modifier l'action preventive actif avec nouvelles données enregistrer.
        */
        public JsonResult ModifierDelaisActionPreventive(string _ListDelais, string _HeureFin, string _MinuteFin)
        {
            
            EdsActionPreventiveModel majActionPrev = actionPreventive;
            majActionPrev.ListeDelai.Clear();

            string[] lst = _ListDelais.Split(';');

            for (int i = 1; i < lst.Length; i++)
            {
                int _Priorite = Convert.ToInt32(lst[i].Substring(2, 2));
                DateTime _DelaisPriorite = Convert.ToDateTime(lst[i].Split('>')[1]);
                //DateTime _DelaisPriorite = Conversion(lst[i].Split('>')[1]);

                DelaiModel newDelais = new DelaiModel()
                {
                    IDActionPreventive = actionPreventive.ID,
                    Priorite = _Priorite,
                    DelaisPriorite = _DelaisPriorite,
                    DtCreation = DateTime.Now
                };
                majActionPrev.ListeDelai.Add(newDelais);
                

                majActionPrev.DtMaj = DateTime.Now;
                majActionPrev.HeureFin += _DelaisPriorite.TimeOfDay;
                
            }
            majActionPrev.ResponsableMaj = codeUser;
               // LoadActionPreventive();
            bool result = Eds.MAJActionPreventive(majActionPrev);
            if (result)
            {
                return Json(new { success = result, responseTitle = "Succès", responseText = "Action de protection mis à jour!." }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                //  Send "Success"
                return Json(new { success = result, responseTitle = "Échec", responseText = "Action de proctection n\'a pas pu être mis à jour!" }, JsonRequestBehavior.AllowGet);
            }
        }

    }
    
}