﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using EtatSituation.Models;
using EtatSituation.API;
using System.Net.Http;
using System;
using Newtonsoft.Json;

namespace EtatSituation.Controllers
{
    public class EdsInterController : Controller
    {

        public async Task<JsonResult> ChargementDonnees()
        {
            List<EdsDonnees> result = Eds.ObtenirDonneesEDS2("inter");

            return Json(new
            {
                CarteAppel = result.Where(a => a.Indicateur.Contains("CarteAppel")).ToList(),
                VehDevoir = result.Where(a => a.Indicateur.Contains("VehDevoir")).ToList(), //VehDevoir  **tempo pour test
                AppelsAttenteAff = result.Where(a => a.Indicateur.Contains("AppelsAttenteAff")).ToList(),
                VolumeAppel = result.Where(a => a.Indicateur.Contains("VolumeAppel")).ToList(),
                SDDF = result.Where(a => a.Indicateur.Contains("SDDF")).ToList(),
                ActionPreventive = result.Where(a => a.Indicateur.Contains("ActionPreventive")).ToList(),
                QuotaEffectifs = result.Where(a => a.Indicateur.Contains("QuotaEffectifs")).ToList()

            });
        }

        public async Task<JsonResult> ChargementMeteo()
        {
            Meteo.MeteoResult meteoResult = Eds.ObtenirMeteo();

            return Json(new
            {
                icon = meteoResult.currently.icon,
                summary = meteoResult.currently.summary,
                temperature = meteoResult.currently.temperature,
                apparentTemperature = meteoResult.currently.apparentTemperature
            });

        }

        // GET: EdsInter
        public ActionResult Index()
        {
            // Chargement de l'entete
            List<EdsDonnees> result = Eds.ObtenirDonneesEDS2("inter");
            var entete = result.Where(a => a.Indicateur.Contains("Entete")).ToList();
            EnteteEDS objEntete = new EnteteEDS { Titre = entete[0].Details };
            return View(objEntete);
        }
    }
}