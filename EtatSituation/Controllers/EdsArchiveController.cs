﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using EtatSituation.Models;
using EtatSituation.API;
using System.Web.UI.WebControls;

namespace EtatSituation.Controllers
{

    public class EdsArchiveController : Controller
    {
        public async Task<JsonResult> ChargementDonnees()
        {
            List<EdsDonnees> result = Eds.ObtenirDonneesEDSArchive("911", "2020-08-06T00:00:00");

            return Json(new
            {
                CarteAppel = result.Where(a => a.Indicateur.Contains("CarteAppel")).ToList(),
                VehDevoir = result.Where(a => a.Indicateur.Contains("VehDevoir")).ToList(), 
                AppelsAttenteAff = result.Where(a => a.Indicateur.Contains("AppelsAttenteAff")).ToList(),
                VolumeAppel = result.Where(a => a.Indicateur.Contains("VolumeAppel")).ToList(),
                SDDF = result.Where(a => a.Indicateur.Contains("SDDF")).ToList(),
                Ratio = result.Where(a => a.Indicateur.Contains("Ratio")).ToList(),
                //ActionPreventive = result.Where(a => a.Indicateur.Contains("ActionPreventive")).ToList(),
                QuotaEffectifs = result.Where(a => a.Indicateur.Contains("QuotaEffectifs")).ToList()
            });
        }

        public async Task<JsonResult> ChargementMeteo()
        {
            Meteo.MeteoResult meteoResult = Eds.ObtenirMeteo();

            return Json(new
            {
                icon = meteoResult.currently.icon,
                summary = meteoResult.currently.summary,
                temperature = meteoResult.currently.temperature,
                apparentTemperature = meteoResult.currently.apparentTemperature
            });

        }

        // GET: EdsArchive
        public ActionResult Index()
        {
            return View();
        }
    }
}