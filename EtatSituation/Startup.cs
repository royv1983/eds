﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EtatSituation.Startup))]
namespace EtatSituation
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
