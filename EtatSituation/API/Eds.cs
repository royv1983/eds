﻿using Newtonsoft.Json;
using EtatSituation.Controllers;
using EtatSituation.Models;

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.UI.WebControls;
using System.Web.ModelBinding;
using Newtonsoft.Json.Converters;

namespace EtatSituation.API
{
    public class Eds
    {
        private HttpClient client;
        //private ListeProcessInstanceModel lstInstance;
        HttpResponseMessage response = null;

       

        ErreurModel erreur;
        ErreurController _erreurController;

       

        public Eds()
        {
            client = new HttpClient();
            client.BaseAddress = new Uri(System.Configuration.ConfigurationManager.ConnectionStrings["APICommun"].ConnectionString + "eds/");
            //if (System.Web.HttpContext.Current.Session["Token"] != null)
            //    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + System.Web.HttpContext.Current.Session["Token"].ToString());
        }

        // *******************************
        // EDS
        // *******************************

       
        public async Task<List<EdsDonnees>> ObtenirDonneesEDS(string type)
        {
            try
            {
                response = await client.GetAsync("ObtenirDonneesEDS/" + type);

                if (response.IsSuccessStatusCode)
                {
                    var jsonString = response.Content.ReadAsStringAsync();
                    jsonString.Wait();
                    return JsonConvert.DeserializeObject<List<EdsDonnees>>(jsonString.Result);
                }
                else
                {
                    var _message = JsonConvert.DeserializeObject<MessageErreurModel>(response.Content.ReadAsStringAsync().Result);
                    erreur = new ErreurModel()
                    {
                        Date = DateTime.Now,
                        Message = _message.MessageResult == null ? response.Content.ReadAsStringAsync().Result : _message.MessageResult,
                        NoErreur = _message.CodeResult,
                        Source = "",
                        Pile = "",
                        CreationUsager = HttpContext.Current.Session["code_user"].ToString()
                    };
                    ErreurController.EnregistrerErreur(erreur);
                }
            }
            catch (Exception ex)
            {
                erreur = new ErreurModel()
                {
                    Date = DateTime.Now,
                    Message = ex.Message,
                    NoErreur = ex.HResult,
                    Source = "",
                    Pile = ex.StackTrace,
                    CreationUsager = HttpContext.Current.Session["code_user"].ToString()
                };
                ErreurController.EnregistrerErreur(erreur);
            }
            return null;
        }

        // sert a obtenir liste Actions 
        public static List<EdsActionModel> ObtenirActionsEDS()
        {
            IEnumerable<EdsActionModel> edsActions = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(System.Configuration.ConfigurationManager.ConnectionStrings["APICommun"].ConnectionString + "eds/");
                //HTTP GET
                var responseTask = client.GetAsync("ObtenirAction/");
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<IList<EdsActionModel>>();
                    readTask.Wait();

                    edsActions = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..

                    edsActions = Enumerable.Empty<EdsActionModel>();

                    //ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }

            return edsActions.ToList();
        }


        public static bool MAJActionPreventive(EdsActionPreventiveModel actionPrev)
        {
            //IEnumerable<EdsActionModel> edsActions = null;
            var json = JsonConvert.SerializeObject(actionPrev);

            using (var client = new HttpClient())
            {
                HttpContent content = new StringContent(json, Encoding.UTF8, "application/json");
                client.BaseAddress = new Uri(System.Configuration.ConfigurationManager.ConnectionStrings["APICommun"].ConnectionString + "eds/");
                //HTTP GET
                var responseTask = client.PostAsync("MAJActionPreventive/", content);
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    
                    var readTask = result.Content.ReadAsStringAsync();
                    readTask.Wait();

                    return result.IsSuccessStatusCode;

                }
                else //web api sent error response 
                {
                    var _message = JsonConvert.DeserializeObject<MessageErreurModel>(result.Content.ReadAsStringAsync().Result);
                    ErreurModel erreur = new ErreurModel()
                    {
                        Date = DateTime.Now,
                        Message = _message.MessageResult == null ? result.Content.ReadAsStringAsync().Result : _message.MessageResult,
                        NoErreur = _message.CodeResult,
                        Source = "",
                        Pile = "",
                        CreationUsager = HttpContext.Current.Session["code_user"].ToString()
                    };
                    ErreurController.EnregistrerErreur(erreur);
                }
            }
            return false;
        }

        // sert a creer Actions prevention
        public static bool AjouterActionPreventive(EdsActionPreventiveModel actionPrev)
        {
            //IEnumerable<EdsActionModel> edsActions = null;
            var json = JsonConvert.SerializeObject(actionPrev);

            using (var client = new HttpClient())
            {
                HttpContent content = new StringContent(json, Encoding.UTF8, "application/json");
                client.BaseAddress = new Uri(System.Configuration.ConfigurationManager.ConnectionStrings["APICommun"].ConnectionString + "eds/");
                //HTTP GET
                var responseTask = client.PostAsync("AjouterActionPreventive/", content);
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {

                    var readTask = result.Content.ReadAsStringAsync();
                    readTask.Wait();

                    return result.IsSuccessStatusCode;
                    
                }
                else //web api sent error response 
                {
                    var _message = JsonConvert.DeserializeObject<MessageErreurModel>(result.Content.ReadAsStringAsync().Result);
                  ErreurModel  erreur = new ErreurModel()
                    {
                        Date = DateTime.Now,
                        Message = _message.MessageResult == null ? result.Content.ReadAsStringAsync().Result : _message.MessageResult,
                        NoErreur = _message.CodeResult,
                        Source = "",
                        Pile = "",
                        CreationUsager = HttpContext.Current.Session["code_user"].ToString()
                    };
                    ErreurController.EnregistrerErreur(erreur);
                }
            }

            return false;
        }

        


        public static EdsActionPreventiveModel ObtenirActionPreventiveActive()
        {
           EdsActionPreventiveModel ActionPreventivesEds = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(System.Configuration.ConfigurationManager.ConnectionStrings["APICommun"].ConnectionString + "eds/");
                //HTTP GET
                var responseTask = client.GetAsync("ObtenirActionPreventiveActive/");
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<EdsActionPreventiveModel>();
                    readTask.Wait();

                    ActionPreventivesEds = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..

                    ActionPreventivesEds = null;

                    //ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }

            return ActionPreventivesEds;
        }

        public static List<EdsPrioriteModel> ObtenirListePriorite()
        {
            List<EdsPrioriteModel> lstPrioriteEds = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(System.Configuration.ConfigurationManager.ConnectionStrings["APICommun"].ConnectionString + "eds/");
                //HTTP GET
                var responseTask = client.GetAsync("ObtenirListePriorite/");
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<List<EdsPrioriteModel>>();
                    readTask.Wait();

                    lstPrioriteEds = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..

                    lstPrioriteEds = null;

                    //ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }

            return lstPrioriteEds;
        }

        public static List<EdsDonnees> ObtenirDonneesEDS2(string type)
        {
            IEnumerable<EdsDonnees> DonneesEDS = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(System.Configuration.ConfigurationManager.ConnectionStrings["APICommun"].ConnectionString + "eds/");
                //HTTP GET
                var responseTask = client.GetAsync("ObtenirDonneesEDS/" + type);
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<IList<EdsDonnees>>();
                    readTask.Wait();

                    DonneesEDS = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..

                    DonneesEDS = Enumerable.Empty<EdsDonnees>();

                    //ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }

            return DonneesEDS.ToList();
        }

        public static List<EdsDonnees> ObtenirDonneesEDSArchive(string type, string rchDate)
        {
            IEnumerable<EdsDonnees> DonneesEDS = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(System.Configuration.ConfigurationManager.ConnectionStrings["APICommun"].ConnectionString + "eds/");
                //HTTP GET
                var responseTask = client.GetAsync("ObtenirDonneesEDSArchive/" + type + "/" + rchDate);
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<IList<EdsDonnees>>();
                    readTask.Wait();

                    DonneesEDS = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..

                    DonneesEDS = Enumerable.Empty<EdsDonnees>();

                    //ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }

            return DonneesEDS.ToList();
        }

        public static Meteo.MeteoResult ObtenirMeteo()
        {
            Meteo.MeteoResult meteoResult = null;


            using (var client = new HttpClient())
            {
                //client.BaseAddress = new Uri("https://api.forecast.io/forecast");
                //HTTP GET
                var responseTask = client.GetAsync("https://api.forecast.io/forecast/157f6942c05675705f0147b59d548e4c/45.5969,-73.5796?units=si&lang=fr&exclude=minutely,hourly,daily,alerts,flags");
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<Meteo.MeteoResult>();
                    readTask.Wait();

                    meteoResult = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..

                    //meteoResult = meteoResult.Empty<Meteo.MeteoResult>();

                    //ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }

            return meteoResult;
        }





    }
}