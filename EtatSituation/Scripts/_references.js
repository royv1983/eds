/// <autosync enabled="true" />
/// <reference path="bootstrap.js" />
/// <reference path="bootstrap-datetimepicker.js" />
/// <reference path="highcharts/adapters/standalone-framework.js" />
/// <reference path="highcharts/adapters/standalone-framework.src.js" />
/// <reference path="highcharts/highcharts.js" />
/// <reference path="highcharts/highcharts.src.js" />
/// <reference path="highcharts/highcharts-3d.js" />
/// <reference path="highcharts/highcharts-3d.src.js" />
/// <reference path="highcharts/highcharts-more.js" />
/// <reference path="highcharts/highcharts-more.src.js" />
/// <reference path="highcharts/modules/boost.js" />
/// <reference path="highcharts/modules/boost.src.js" />
/// <reference path="highcharts/modules/broken-axis.js" />
/// <reference path="highcharts/modules/broken-axis.src.js" />
/// <reference path="highcharts/modules/canvas-tools.js" />
/// <reference path="highcharts/modules/canvas-tools.src.js" />
/// <reference path="highcharts/modules/data.js" />
/// <reference path="highcharts/modules/data.src.js" />
/// <reference path="highcharts/modules/drilldown.js" />
/// <reference path="highcharts/modules/drilldown.src.js" />
/// <reference path="highcharts/modules/exporting.js" />
/// <reference path="highcharts/modules/exporting.src.js" />
/// <reference path="highcharts/modules/funnel.js" />
/// <reference path="highcharts/modules/funnel.src.js" />
/// <reference path="highcharts/modules/heatmap.js" />
/// <reference path="highcharts/modules/heatmap.src.js" />
/// <reference path="highcharts/modules/no-data-to-display.js" />
/// <reference path="highcharts/modules/no-data-to-display.src.js" />
/// <reference path="highcharts/modules/offline-exporting.js" />
/// <reference path="highcharts/modules/offline-exporting.src.js" />
/// <reference path="highcharts/modules/solid-gauge.js" />
/// <reference path="highcharts/modules/solid-gauge.src.js" />
/// <reference path="highcharts/modules/treemap.js" />
/// <reference path="highcharts/modules/treemap.src.js" />
/// <reference path="highcharts/themes/dark-blue.js" />
/// <reference path="highcharts/themes/dark-green.js" />
/// <reference path="highcharts/themes/dark-unica.js" />
/// <reference path="highcharts/themes/gray.js" />
/// <reference path="highcharts/themes/grid.js" />
/// <reference path="highcharts/themes/grid-light.js" />
/// <reference path="highcharts/themes/sand-signika.js" />
/// <reference path="highcharts/themes/skies.js" />
/// <reference path="jquery.unobtrusive-ajax.js" />
/// <reference path="jquery.validate.js" />
/// <reference path="jquery.validate.unobtrusive.js" />
/// <reference path="jquery-2.2.0.js" />
/// <reference path="jquery-ui-1.11.4/external/jquery/jquery.js" />
/// <reference path="jquery-ui-1.11.4/jquery-ui.js" />
/// <reference path="less-1.4.1.min.js" />
/// <reference path="modernizr-2.8.3.js" />
/// <reference path="moment.js" />
/// <reference path="moment-with-locales.js" />
/// <reference path="respond.js" />
/// <reference path="respond.matchmedia.addListener.js" />
