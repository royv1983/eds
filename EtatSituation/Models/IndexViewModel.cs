﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EtatSituation.Models
{
    public class IndexViewModel
    {
        public string Titre { get; set; }
        public List<EdsActionModel> Actions { get; set; }
        public EdsActionPreventiveModel ActionPreventive { get; set; }
        public List<string> Securite { get; set; }
        public string User { get; set; }
        public List<DelaiModel> Delais { get; set; }
        public List<EdsPrioriteModel> Priorite { get; internal set; }

        public string Token { get; set; }

    }
}