﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace EtatSituation.Models
{
    public class ErreurModel
    {
        #region Attributs

        public long Id { get; set; }
        public DateTime Date { get; set; }
        public string Source { get; set; }
        public int NoErreur { get; set; }
        public string Message { get; set; }
        public string Pile { get; set; }
        public string CreationUsager { get; set; }
        public string AdresseIp { get; set; }
        public string Noeud { get; set; }

        #endregion

        #region Constructeur
        public ErreurModel()
        {
            Noeud = System.Environment.MachineName;
            AdresseIp = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            CreationUsager = HttpContext.Current.Session["code_user"] != null ? HttpContext.Current.Session["code_user"].ToString() : "";
        }
        public ErreurModel(Exception err)
        {
            if (err != null)
            {
                NoErreur = err.HResult;
                Date = DateTime.Now;
                Source = err.Source;
                Message = err.Message;
                Pile = err.StackTrace;
            }
            else
            {
                NoErreur = -1;
                Date = DateTime.Now;
                Source = "Erreur null";
                Message = "Erreur null";
                Pile = "Erreur null";
            }

            Noeud = System.Environment.MachineName;
            AdresseIp = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            try
            {
                CreationUsager = HttpContext.Current.Session["code_user"] != null ? HttpContext.Current.Session["code_user"].ToString() : "";
            }
            catch
            {
                CreationUsager = "";
            }
        }
        #endregion
    }
}