﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EtatSituation.Models
{
    public class Delais
    {
        public int ID { get; set; }
        public int IDActionPreventive { get; set; }
        public int Priorite { get; set; }
        public DateTime DelaisPriorite { get; set; }
        public DateTime DtCreation { get; set; }

    }
}