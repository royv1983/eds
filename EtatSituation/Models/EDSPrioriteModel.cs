﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EtatSituation.Models
{
    public class EdsPrioriteModel
    {
            public int ID { get; set; }
            public int Priorite_cd { get; set; }
            public string Priorite_tx { get; set; }
            public MessageErreurModel Erreur { get; set; }
        
    }
}