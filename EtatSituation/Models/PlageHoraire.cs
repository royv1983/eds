﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EtatSituation.Models
{
    public class PlageHoraire
    {
        public int ID { get; set; }
        public DateTime DtJour { get; set; }
        public int No_Jour { get; set; }
        public string Code_Plage { get; set; }
        public int Nbr_Comble { get; set; }
        public int Nb_pers_min { get; set; }
        public int PlageOrder { get; set; }
        public int? Statut { get; set; }
    }
}