﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EtatSituation.Models
{
    public class EdsActionModel
    {
        public int ID { get; set; }
        public string Description { get; set; }

        public MessageErreurModel Erreur { get; set; }
    }
}