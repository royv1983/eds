﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EtatSituation.Models
{
    public class Relais
    {
        public int ID { get; set; }
        public int IDActionPreventive { get; set; }
        public string Description { get; set; }
    }
}