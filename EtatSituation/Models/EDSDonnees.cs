﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EtatSituation.Models
{
    public class EdsDonnees
    {
        public string Indicateur { get; set; }
        public string Details { get; set; }
        public int? Ordre { get; set; }
        public int? Val1 { get; set; }
        public int? Val2 { get; set; }
        public int? Val3 { get; set; }
        public int? Val4 { get; set; }
        public string Texte1 { get; set; }
        public string Texte2 { get; set; }
        public string Texte3 { get; set; }
        public string Texte4 { get; set; }
        public string Texte5 { get; set; }
        public MessageErreurModel Erreur { get; set; }
    }
}