﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EtatSituation.Models.SecuriteOpenID
{
    public class UtilisateurUS
    {
        public string Matricule { get; set; }
        public string CdUsager { get; set; }
        public string Courriel { get; set; }
        public string Nom { get; set; }
        public string groups { get; set; }

        public string AccessToken { get; set; }
    }
}