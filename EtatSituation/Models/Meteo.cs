﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EtatSituation.Models
{
    public class Meteo
    {
          
        public class MeteoResult
        {
            public decimal lat { get; set; }
            public decimal lon { get; set; }
              public string timezone { get; set; }
            public int offset { get; set; }
            public TempJson currently;
            
           
        }

        
        public class TempJson
        {
            public string time { get; set; }
            public string summary { get; set; }
            public string icon { get; set; }
            public int nearestStormDistance { get; set; } 
            public int nearestStormBearing { get; set; }
            public double precipIntensity { get; set; }
            public double temperature { get; set; }
            public double apparentTemperature { get; set; }
            public double dewPoint { get; set; }
            public double humidity { get; set; }
            public double windSpeed { get; set; }
            public double windBearing { get; set; }
            public double visibility { get; set; }
            public double cloudCover { get; set; }
            public double pressure { get; set; }
            public double ozone { get; set; }

           }
 
    }
}