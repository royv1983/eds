﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EtatSituation.Models
{
    /// <summary>
    /// MessageErreur
    /// </summary>
    public class MessageErreurModel
    {
        /// <summary>
        /// Code d'erreur
        /// </summary>
        public int CodeResult { get; set; }
        /// <summary>
        /// Message d'erreur
        /// </summary>
        public string MessageResult { get; set; }
        /// <summary>
        ///  Message de retour
        /// </summary>
        /// <param name="CodeResult"></param>
        /// <param name="MessageResult"></param>
        /// <returns></returns>
        public MessageErreurModel MessageErreurResultat(int CodeResult, string MessageResult)
        {
            this.CodeResult = CodeResult;
            this.MessageResult = MessageResult;
            return this;
        }
    }
}