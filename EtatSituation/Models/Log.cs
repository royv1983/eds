﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EtatSituation.Models
{
    public class Log
    {
        public int ID { get; set; }
        public string Utilisateur { get; set; }
        public string Type { get; set; }
        public DateTime Date { get; set; }
        public string  Description { get; set; }
    }
}