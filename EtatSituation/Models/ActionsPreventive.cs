﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EtatSituation.Models
{
    public class ActionsPreventive
    {
        public int ID { get; set; }
        public int IDAction { get; set; }
        public DateTime HeureDebut { get; set; }
        public DateTime? HeureFin { get; set; }
        public bool? Statut { get; set; }
        public DateTime DtCreation { get; set; }
        public DateTime? DtMaj { get; set; }
    }
}