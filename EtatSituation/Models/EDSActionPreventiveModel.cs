﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EtatSituation.Models
{
    public class EdsActionPreventiveModel
    {
        public int ID { get; set; }
        public int IDAction { get; set; }
        public string DescriptionAction { get; set; }
        public DateTime HeureDebut { get; set; }
        public DateTime HeureFin { get; set; }
        public Boolean Statut { get; set; }
        public DateTime DtCreation { get; set; }
        public string ResponsableCreation { get; set; }
        public DateTime? DtMaj { get; set; }
        public string ResponsableMaj { get; set; }
        public List<DelaiModel> ListeDelai { get; set; }
        public MessageErreurModel Erreur { get; set; }
    }
}