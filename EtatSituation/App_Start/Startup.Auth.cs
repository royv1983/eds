﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.IdentityModel.Protocols;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OpenIdConnect;
using System;

using Microsoft.Owin.Security.DataProtection;
using Owin;
using EtatSituation.Models;


using System.IdentityModel.Tokens;
using System.Threading.Tasks;

using System.Configuration;
using System.Diagnostics;

namespace EtatSituation
{
    public partial class Startup
    {
        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuth(IAppBuilder app)
        {

            //Configurer le contexte de base de données, le gestionnaire des utilisateurs et le gestionnaire des connexions pour utiliser une instance unique par demande
            app.CreatePerOwinContext(ApplicationDbContext.Create);
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);
            app.CreatePerOwinContext<ApplicationSignInManager>(ApplicationSignInManager.Create);

            // Laisser l'application utiliser un cookie pour stocker les informations de l'utilisateur connecté
            // et d'utiliser un cookie pour stocker temporairement des informations sur une connexion utilisateur avec un fournisseur de connextion tiers
            // Configurer le cookie d'inscription
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                //  LoginPath = new PathString("/Account/Login"),
                Provider = new CookieAuthenticationProvider
                {
                    OnValidateIdentity = SecurityStampValidator.OnValidateIdentity<ApplicationUserManager, ApplicationUser>(
                        validateInterval: TimeSpan.FromMinutes(30),
                        regenerateIdentity: (manager, user) => user.GenerateUserIdentityAsync(manager))
                }
            });
            // Utilisez un cookie pour stocker temporairement des informations sur une connexion utilisateur avec un fournisseur de connexion tiers
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            // Permet à l'application de stocker temporairement les informations utilisateur lors de la vérification du second facteur dans le processus d'authentification à 2 facteurs.
            //app.UseTwoFactorSignInCookie(DefaultAuthenticationTypes.TwoFactorCookie, TimeSpan.FromMinutes(5));

            // Permet à l'application de mémoriser le second facteur de la vérification, un numéro de téléphone ou un e-mail par exemple.
            // Lorsque vous sélectionnez cette option, votre seconde étape de vérification lors du processus de connexion est mémorisée sur le poste à partir duquel vous vous êtes connecté.
            // Ceci est similaire à l'option RememberMe lors de la connexion.
            //app.UseTwoFactorRememberBrowserCookie(DefaultAuthenticationTypes.TwoFactorRememberBrowserCookie);

            var options = new OpenIdConnectAuthenticationOptions()
            {
                ClientId = ConfigurationManager.AppSettings["OicClientId"],
                ClientSecret = ConfigurationManager.AppSettings["OicClientSecret"],
                Authority = ConfigurationManager.AppSettings["OicAuthorityURL"],
                RedirectUri = ConfigurationManager.AppSettings["OicRedirectURL"],
                AuthenticationMode = AuthenticationMode.Active,
                Scope = "openid CdUsagerScope",
                ResponseType = "code",
                
                AuthenticationType = "authorization_code",
                TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = false,
                    SaveSigninToken = true,
                },
                Notifications = new OpenIdConnectAuthenticationNotifications()
                {
                    RedirectToIdentityProvider = (context) =>
                    {
                        Debug.WriteLine("*** RedirectToIdentityProvider");
       //                 string appBaseUrl = context.Request.Scheme + "://"
       //+ context.Request.Host + context.Request.PathBase;
       //                 context.ProtocolMessage.RedirectUri = appBaseUrl + "/";
       //                 context.ProtocolMessage.PostLogoutRedirectUri = appBaseUrl;
                       
                        return Task.FromResult(0);
                    },
                    MessageReceived = (context) =>
                    {
                        Debug.WriteLine("*** MessageReceived");
                        return Task.FromResult(0);
                    },
                    SecurityTokenReceived = (context) =>
                    {
                        Debug.WriteLine("*** SecurityTokenReceived");
                        return Task.FromResult(0);
                    },
                    SecurityTokenValidated = (context) =>
                    {
                        Debug.WriteLine("*** SecurityTokenValidated");
                        return Task.FromResult(0);
                    },
                    AuthorizationCodeReceived = (context) =>
                    {
                        Debug.WriteLine("*** AuthorizationCodeReceived");
                        return Task.FromResult(0);
                    },
                    AuthenticationFailed = (context) =>
                    {
                        Debug.WriteLine("*** AuthenticationFailed");
                        return Task.FromResult(0);
                    },
                }

            };

            options.Configuration = new OpenIdConnectConfiguration
            {
                AuthorizationEndpoint = ConfigurationManager.AppSettings["OicAuthorityURL"] + "oauth2/authorize",
                JwksUri = ConfigurationManager.AppSettings["OicAuthorityURL"] + "jwks",
                TokenEndpoint = ConfigurationManager.AppSettings["OicAuthorityURL"] + "oauth2/token",
                UserInfoEndpoint = ConfigurationManager.AppSettings["OicAuthorityURL"] + "oauth2/userinfo",
                Issuer = ConfigurationManager.AppSettings["OicAuthorityURL"],
                EndSessionEndpoint = ConfigurationManager.AppSettings["OicAuthorityURL"] + "oauth2/logout",
                CheckSessionIframe = ConfigurationManager.AppSettings["OicAuthorityURL"] + "oauth2/checksession",
            };

            app.UseOpenIdConnectAuthentication(options);
        }
    }
}